module.exports = {
  publicPath: './',
  devServer: {
    port: 8080,
    open: true,
    overlay: {
      warnings: false,
      errors: true
    },
    proxy: process.env.VUE_APP_BASE_API
  }
}
