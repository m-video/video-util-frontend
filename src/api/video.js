import fetch from '../util/fetch'

export function parseVideo(params) {
  return fetch({
    url: `api/video`,
    method: 'get',
    params
  })
}
