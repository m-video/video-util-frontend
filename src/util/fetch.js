/*eslint no-console: ["error", { allow: ["log"] }] */

import axios from 'axios'
import { Toast } from 'vant'

const axiosConfig = {
  timeout: 3000 // 请求超时时间
}
if (process.env.NODE_ENV === 'production') {
  axiosConfig.baseURL = process.env.VUE_APP_BASE_API // api的base_url
}

// 创建axios实例
const service = axios.create(axiosConfig)

// request拦截器
service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    Toast({
      message: `请求出错：${error.message}`,
      duration: 2000
    })
    Promise.reject(error)
  }
)

// respone拦截器
service.interceptors.response.use(
  response => {
    return response.data
  },
  error => {
    console.log(error)
    let { message } = error.response.data
    message = message || error.message
    Toast({
      message: `请求出错：${message}`,
      position: 'bottom',
      duration: 2000
    })
    return Promise.reject(error)
  }
)

export default service
