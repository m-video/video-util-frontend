import Vue from 'vue'
import App from './App.vue'
import './style/main.less'
import { Lazyload } from 'vant'

// options 为可选参数，无则不传
Vue.use(Lazyload)

Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
