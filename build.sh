name="vu-frontend"

npm run build

if [ -d $name ]
then
    rm -rf ${name}
fi

mv ./dist ${name}

tar -zcvf ${name}.tar.gz ${name}

scp ${name}.tar.gz root@ax:/root/app